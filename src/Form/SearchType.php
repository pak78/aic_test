<?php declare(strict_types=1);

namespace App\Form;

use App\Manager\AirportsManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchType.
 */
class SearchType extends AbstractType
{
    public function __construct(private AirportsManager $airportsManager)
    {
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromAirport', ChoiceType::class, [
                'choices' => $this->airportsManager->getAirportsArray(),
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('toAirport', ChoiceType::class, [
                'choices' => $this->airportsManager->getAirportsArray(),
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('dateTrip', DateType::class, [
                'widget' => 'single_text',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }
}
