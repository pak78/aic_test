<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Airport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AirportRepository.
 */
class AirportRepository extends ServiceEntityRepository
{
    /**
     * AirportRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Airport::class);
    }

    public function getAllAirportsArray(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb
            ->select('air')
            ->from(Airport::class, 'air')
            ->orderBy('air.name')
            ->getQuery()
        ;

        return $query->getArrayResult();
    }
}
