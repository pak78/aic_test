<?php declare(strict_types=1);

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AmadeusClient.
 */
final class AmadeusClient extends Client
{
    private const API_URL = 'https://test.api.amadeus.com';
    private const SET_TIMEOUT = 500;

    /**
     * @param string $key
     * @param string $secret
     */
    public function __construct(private string $key, private string $secret)
    {
        parent::__construct(
            [
                'http_error' => true,
                'connect_timeout' => self::SET_TIMEOUT,
                'timeout' => self::SET_TIMEOUT,
            ]
        );
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $date
     *
     * @return array
     *
     * @throws GuzzleException
     */
    public function getFlightDestinations(string $from, string $to, string $date): array
    {
        $url = \sprintf('%s/v2/shopping/flight-offers', self::API_URL);
        $params = [
            'query' => [
                'originLocationCode' => $from,
                'destinationLocationCode' => $to,
                'departureDate' => $date,
                'adults' => '1',
            ],
            'headers' => [
                'Authorization' => \sprintf('Bearer %s', $this->getToken()),
            ],
        ];

        $response = $this->request(Request::METHOD_GET, $url, $params);
        $content = $response->getBody()->getContents();
        $data = \GuzzleHttp\json_decode($content, true);

        return $data['data'];
    }

    /**
     * @return string
     *
     * @throws GuzzleException
     */
    private function getToken(): string
    {
        $params = [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $this->key,
                'client_secret' => $this->secret,
            ],
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ];

        $url = \sprintf('%s/v1/security/oauth2/token', self::API_URL);

        $response = $this->request(Request::METHOD_POST, $url, $params);
        $content = $response->getBody()->getContents();
        $data = \GuzzleHttp\json_decode($content, true);

        return $data['access_token'];
    }
}
