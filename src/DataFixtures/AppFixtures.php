<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Airport;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $csvFile = \file(__DIR__ . '/../Catalogs/airports.csv');
        $airports = [];
        foreach ($csvFile as $line) {
            $airports[] = \str_getcsv($line, ';');
        }

        \array_map(function ($airport) use ($manager) {
            $air = new Airport();
            $air->setIataCode($airport[0]);
            $air->setName($airport[1]);
            $air->setCity($airport[2]);
            $air->setCountry($airport[3]);
            $manager->persist($air);
        }, $airports);

        $manager->flush();

        UserFactory::createOne(['email' => 'admin@admin.com']);
    }
}
