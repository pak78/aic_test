<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class InvalidIataCodeException extends \Exception
{
    public function __construct($message = 'Invalid iata code.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
