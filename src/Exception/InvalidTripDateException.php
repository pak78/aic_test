<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class InvalidTripDateException extends \Exception
{
    public function __construct($message = 'Invalid date. Is in the past', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
