<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class BadRequestException extends \Exception
{
    public function __construct($message = 'Bad request.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
