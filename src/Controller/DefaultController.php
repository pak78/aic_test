<?php declare(strict_types=1);

namespace App\Controller;

use App\Form\SearchType;
use App\Manager\FlightsManager;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends AbstractController
{
    /**
     * @param FlightsManager $flightsManager
     */
    public function __construct(private FlightsManager $flightsManager)
    {
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @throws \Exception
     */
    public function logout()
    {
        throw new \Exception('logout() should never be reached');
    }

    /**
     * @return Response
     */
    public function search(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $form = $this->createForm(SearchType::class);

        return $this->render('default/search.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @return JsonResponse
     *
     * @throws GuzzleException
     */
    public function getFlights(): JsonResponse
    {
        return $this->json(['flights' => $this->flightsManager->getFlightsByDates()]);
    }
}
