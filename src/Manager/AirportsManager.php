<?php declare(strict_types=1);

namespace App\Manager;

use App\Repository\AirportRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;

/**
 * Class AirportsManager.
 */
class AirportsManager
{
    public const CACHE_KEY = 'aic.airports';

    /**
     * AirportsManager constructor.
     *
     * @param AdapterInterface  $cache
     * @param AirportRepository $airportRepository
     */
    public function __construct(
        private AdapterInterface $cache,
        private AirportRepository $airportRepository
    ) {
    }

    public function getAirportsArray(): array
    {
        $airports = \json_decode($this->getAirports(), true);

        $airportArray = [];

        \array_walk($airports, function ($airport) use (&$airportArray) {
            $airportArray[$airport['name']] = $airport['iataCode'];
        });

        return $airportArray;
    }

    /**
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function getAirports(): string
    {
        $cacheAirports = $this->cache->getItem(self::CACHE_KEY);

        try {
            if (!$cacheAirports->isHit()) {
                $airports = $this->airportRepository->getAllAirportsArray();
                $cacheAirports->set(\json_encode($airports));
                $this->cache->save($cacheAirports);
            }
        } catch (\Exception) {
        }

        return $cacheAirports->get();
    }
}
