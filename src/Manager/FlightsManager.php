<?php declare(strict_types=1);

namespace App\Manager;

use App\Clients\AmadeusClient;
use App\Exception\InvalidTripDateException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Flights.
 */
class FlightsManager
{
    /**
     * @param RequestStack $requestStack
     */
    public function __construct(private RequestStack $requestStack, private AmadeusClient $client)
    {
    }

    /**
     * @return array
     *
     * @throws GuzzleException
     */
    public function getFlightsByDates(): array
    {
        try {
            $request = $this->requestStack->getMainRequest();
            $data = $request->request->all();

            if (\strtotime($data['dateTrip']) < time()) {
                throw new InvalidTripDateException();
            }

            $flights = $this->client->getFlightDestinations($data['toAirport'], $data['fromAirport'], $data['dateTrip']);

            $flights = \array_map(function ($flight) {
                $data = $flight['itineraries'][0];
                $duration = new \DateInterval($data['duration']);

                $flightData = [];

                \array_map(function ($segment) use (&$flightData) {
                    $flightData[] = [
                        'carrier' => $segment['carrierCode'],
                        'flyNumber' => $segment['number'],
                        'departureTime' => $segment['departure']['at'],
                        'arrivalTime' => $segment['arrival']['at'],
                    ];
                }, $data['segments']);

                return [
                    'flightData' => $flightData,
                    'duration' => (int) $duration->format('%h') * 60 + (int) $duration->format('%i'),
                ];
            }, $flights);

            $duration = \array_column($flights, 'duration');

            \array_multisort($duration, SORT_ASC, $flights);

            return $flights;
        } catch (\Exception) {
            return [];
        }
    }
}
