<?php

return PhpCsFixer\Config::create()
   ->setRules([
           '@Symfony' => true,
           'strict_param' => true,
           'phpdoc_no_empty_return' => false,
           'concat_space' => ['spacing' => 'one'],
           'array_syntax' => ['syntax' => 'short'],
           'blank_line_after_opening_tag' => false,
           'general_phpdoc_annotation_remove' => false,
           'no_superfluous_phpdoc_tags' => false,
           'linebreak_after_opening_tag' => false,
       ])
    ->setUsingCache(false)
    ->setRiskyAllowed(true)
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__ . "/src")
    );
;