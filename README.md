## Installation

```build
docker-compose build
docker-compose up -d
```

```db 
docker-compose exec aic_app php bin/console doctrine:database:create 
docker-compose exec aic_app php bin/console doctrine:migration:migrate 
docker-compose exec aic_app php bin/console doctrine:fixtures:load --no-interaction
```


## Usage
```
host: localhost:8888
login:
    user: admin@admin.com
    pwd: admin
```
