$(document).ready(function() {
    $('select').selectpicker();

    $('button.search').on('click', function(e) {
        e.preventDefault();

        let data= {
            toAirport: $('#search_toAirport').val(),
            fromAirport: $('#search_fromAirport').val(),
            dateTrip: $('#search_dateTrip').val()
        };

        $.ajax({
            method: 'POST',
            data: data,
            url: '/get-flights',
            dataType: "json",
            encode: true,
        }).done(function (data) {
            $('#flyTable').css("display", "inline-table");

            if(data['flights'].length){
                $('#flyTable tbody td').remove();

                data['flights'].forEach(function(item, i, arr) {
                    let length = item['flightData'].length;
                    let row = '';
                    item['flightData'].forEach(function(it, i, arr) {
                        if(i == 0){
                            row += '<tr>'+
                                '<td rowspan="'+ length +'">' + item['duration'] + '</td>'+
                                '<td>' + it['carrier'] + '</td>'+
                                '<td>' + it['flyNumber'] + '</td>'+
                                '<td>' + it['departureTime'] + '</td>'+
                                '<td>' + it['arrivalTime'] + '</td>' +
                                '</tr>';
                        }else{
                            row += '<tr>'+
                                '<td>' + it['carrier'] + '</td>'+
                                '<td>' + it['flyNumber'] + '</td>'+
                                '<td>' + it['departureTime'] + '</td>'+
                                '<td>' + it['arrivalTime'] + '</td>' +
                                '</tr>';
                        }
                    });
                    $('#flyTable tbody').append(row);
                });
            }else{
                $('#flyTable tbody td').remove();
                $('#flyTable tbody').append('<tr><td colspan="5">no flights</td></tr>');
            }
        });
    });
});
